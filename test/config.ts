import { assert } from "chai";
import * as _ from "lodash";
import "mocha";
import * as config from "../src/lib/config";
import * as logger from "../src/lib/logger";
import * as run from "../src/lib/run";
import { arrayContains, assertThrowsAsync } from "../src/lib/test-util";

logger.silent();

describe("config.middleware()", () => {
    it("should load schedic.yml with one repo", async () => {
        await config.middleware({
            config: "schedic.yml",
            repo: ["mycloud"],
        });
        assert.equal(config.repos().length, 1);
    });

    it("should load schedic.yml with one host", async () => {
        await config.middleware({
            config: "schedic.yml",
            host: ["homesrv"],
        });
        assert.equal(config.hosts().length, 1);
    });

    it("should load schedic.yml with one repo and host", async () => {
        await config.middleware({
            config: "schedic.yml",
            host: ["homesrv"],
            repo: ["mycloud"],
        });
        assert.equal(config.repos().length, 1);
        assert.equal(config.hosts().length, 1);
    });

    it("should load schedic.yml with one repo, host and path", async () => {
        await config.middleware({
            config: "schedic.yml",
            host: ["homesrv"],
            path: ["/etc"],
            repo: ["mycloud"],
        });
        assert.equal(config.repos().length, 1);
        assert.equal(config.hosts().length, 1);
        assert.equal(config.backups().length, 1);
    });

    it("should load schedic.yml and complain about empty backup list", async () => {
        await assertThrowsAsync(async () => {
            await config.middleware({
                config: "schedic.yml",
                path: ["/non-existent"],
            });
        }, /no matching backups/);
    });

    it("should load bad-version.yml and complain about unsupported version", async () => {
        await assertThrowsAsync(async () => {
            await config.middleware({
                config: "test/assets/bad-version.yml",
            });
        }, /not supported/);
    });

    it("should load bad-schema.yml and complain about schema error", async () => {
        await assertThrowsAsync(async () => {
            await config.middleware({
                config: "test/assets/bad-schema.yml",
            });
        }, /schema validation failure/);
    });

    it("should load multi-errors.yml and complain about all errors", async () => {
        logger.clearBuffer();
        logger.buffer();

        await assertThrowsAsync(async () => {
            await config.middleware({
                config: "test/assets/multi-errors.yml",
            });
        }, /configuration errors/);

        const buffer = logger.getBuffer();

        assert.ok(arrayContains(buffer, /multiple backup path.*etc/));
        assert.ok(arrayContains(buffer, /unknown repo.*for host/));
        assert.ok(arrayContains(buffer, /unknown repo.*in "backup-defaults"/));
        assert.ok(arrayContains(buffer, /repo.*password/));
    });

    it("should load complete example schedic.yml", async () => {
        await config.middleware({config: "schedic.yml"});
        assert.equal(config.version(), "2.0", "version 2.0");
    });
});

describe("config.repos()", () => {
    it("should return three repos", () => {
        assert.equal(config.repos().length, 3);
    });
});

describe("config.repo()", () => {
    it("should get mycloud repo", () => {
        const mycloud = config.repo("mycloud");
        assert.equal(mycloud.name, "mycloud");
        assert.equal(mycloud.url, "rest:https://mycloud.example.com");
        assert.equal(mycloud["password-file"], "test/assets/.restic-mycloud.pw");
        assert.equal(mycloud.password, "this is a bad example password to make the example schedic.yml pass");
        assert.deepEqual(mycloud.restic, {"limit-upload": 900});
    });

    it("should get local repo", () => {
        const local = config.repo("local");
        assert.equal(local.name, "local");
        assert.equal(local.url, "/storage/backup/restic");
        assert.equal(local["password-file"], undefined);
        assert.equal(local.password, "super secret local restic password");
    });

    it("should get s3 repo", () => {
        const s3 = config.repo("s3");
        assert.equal(s3.name, "s3");
        assert.equal(s3.url, "s3:s3.amazonaws.com/myresticbucket");
        assert.equal(s3.password, "mys3awsresticpassword");
        assert.equal(s3.env.AWS_ACCESS_KEY_ID, "myawsaccesskey");
        assert.equal(s3.env.AWS_SECRET_ACCESS_KEY, "example s3 aws password for test-suite");
    });

    it("should throw on unknown repo", () => {
        assert.throw(() => { config.repo("boo"); }, /Unknown repo/);
    });
});

describe("config.hosts()", () => {
    it("should return two hosts", () => {
        assert.equal(config.hosts().length, 2);
    });
});

describe("config.host()", () => {
    it("should get homesrv host", () => {
        const homesrv = config.host("homesrv");
        assert.equal(homesrv.name, "homesrv");
        assert.equal(homesrv.arch, "linux_amd64");
        assert.equal(homesrv.ssh, false);
        assert.match(homesrv.pre[0], /gitlab/);
        assert.equal(homesrv.backups.length, 6);
        assert.equal(homesrv.restic["limit-upload"], 500);
        assert.equal(homesrv.backups[0].restic["one-file-system"], true);
    });

    it("should get raspberry1 host", () => {
        const raspberry1 = config.host("raspberry1");
        assert.equal(raspberry1.name, "raspberry1");
        assert.equal(raspberry1.arch, "linux_arm");
        assert.equal(raspberry1.ssh, "ssh -o 'BatchMode Yes'");
        assert.equal(raspberry1.backups.length, 3);
        assert.equal(raspberry1.restic["limit-upload"], 500);
        assert.equal(raspberry1.backups[0].restic["one-file-system"], true);
    });

    it("should throw on unknown host", () => {
        assert.throw(() => { config.host("boo"); }, /Unknown host/);
    });
});

describe("config.backups()", () => {
    it("should return configured backups", () => {
        const backups = config.backups();
        assert.equal(backups.length, 9);
        assert.equal(backups[0].keep.daily, 3);
        assert.equal(backups[5].keep.daily, undefined);
        assert.equal(backups[5].keep.weekly, 2);
    });
});

describe("config env", () => {
    it("should be composed correctly from backup.yml", async () => {
        const argv = {
            config: "test/assets/backup.yml",
            host: ["ci1"],
            repo: ["local"],
        };

        await config.middleware(argv);

        logger.clearBuffer();
        logger.buffer();

        const host = config.host("ci1");
        const repo = config.repo("local");

        const rv = await run.restic("env|sort; echo Q_UOTTEST=$QUOTTEST; echo FINISHED", host, repo);

        assert.match(rv.stdout, /REPO_VAR=repo_var/, "1");
        assert.match(rv.stdout, /REPO_FILE=.*bad example password/, "2");
        assert.match(rv.stdout, /REPO_HOST_OVERRIDDEN=overridden-by-host/, "3");
        assert.match(rv.stdout, /REPO_HOST_DEF_OVERRIDDEN=overridden-by-host-def/, "4");
        assert.match(rv.stdout, /HOST_DEF_VAR=host_def_var/, "5");
        assert.match(rv.stdout, /HOST_DEF_FILE=.*bad example password/, "6");
        assert.match(rv.stdout, /HOST_DEF_OVERRIDDEN=overridden-by-host/, "7");
        assert.match(rv.stdout, /HOST_VAR=host_var/, "8");
        assert.match(rv.stdout, /HOST_FILE=.*bad example password/, "9");
        assert.match(rv.stdout, /Q_UOTTEST=some'quotes and \\slashes and \$ollars/, "10");
        assert.match(rv.stdout, /FINISHED/);
    });
});
