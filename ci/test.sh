#!/bin/bash
set -e

if [ ! -e .cache/bin/restic ]
then
    echo "* Downloading and extracting restic"
    mkdir -p .cache/bin
    curl -sL https://github.com/restic/restic/releases/download/v0.9.4/restic_0.9.4_linux_amd64.bz2 \
        | bunzip2 - > .cache/bin/restic
    chmod 755 .cache/bin/restic
fi

export PATH="$PWD/.cache/bin:$PATH"
export RESTIC_TEST=1

yarn cover
