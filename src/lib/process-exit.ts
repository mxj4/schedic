import * as ChildProcess from "child_process";

export default function processExit(childProcess: ChildProcess.ChildProcess): Promise<number> {
    return new Promise( (resolve) => {
        childProcess.on("close", (code) => {
            resolve(code);
        });
    });
}
