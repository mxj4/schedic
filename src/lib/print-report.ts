import chalk from "chalk";
import { getBorderCharacters, table } from "table";
import * as logger from "../lib/logger";

export default function printReport(report: string[][], title: string, groupCol: number, columns) {
    // Header is bold
    for ( let i = 0; i < report[0].length; ++i ) {
        report[0][i] = chalk.bold(report[0][i]);
    }

    const lastCol = report[0].length - 1;

    let hasErrors = false;
    let hasOk = false;
    for ( let i = 1; i < report.length; ++i ) {
        const error = report[i][lastCol] === "Error";
        if ( error) { hasErrors = true; }
        if ( !error) { hasOk = true; }
        report[i][lastCol] = error ?
            chalk.red.bold(report[i][lastCol]) :
            chalk.green.bold(report[i][lastCol]);
    }

    const config = {
        border: getBorderCharacters(
            process.stdout.isTTY ? "honeywell" : "ramac",
        ),
        columns,
        drawHorizontalLine : (index, size) => {
            if ( index < 2 || index === size ) { return true; }
            if ( groupCol !== null ) {
                return report[index][groupCol] !== report[index - 1][groupCol];
            }
            else {
                return false;
            }
        },
    };

    const successMsg =
        !hasOk    ? chalk.red("ERROR: All tasks failed") :
        hasErrors ? chalk.red("ERROR: Some tasks failed") :
                    chalk.green("OK: All tasks successfully completed");

    logger.print("");
    logger.print(chalk.bold("> " + title));
    logger.print("> " + successMsg);
    logger.print(table(report, config));
}
