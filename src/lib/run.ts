import * as ChildProcess from "child_process";
import * as config from "../lib/config";
import * as logger from "../lib/logger";
import processExit from "../lib/process-exit";
import * as util from "../lib/util";

export type RunResult = {
    exitCode: number,
    stderr: string,
    stdout: string,
};

export type RunOptions = {
    input?: string,
    silent?: boolean,
};

export async function restic(
    cmd: string, host: config.Host, repo: config.Repo,
    options?: RunOptions): Promise<RunResult>
{
    if ( !options ) { options = {};  }
    const sshOptions: any = options as any;

    if ( repo.env || host.env ) {
        const env = await config.mergeEnv(repo.env, host.env);

        // Now we build a bash construct to receive the password
        // in the first line and then all env vars in a pipe and
        // export env to the restic command. Nothing is exposed
        // in the process list this way.
        cmd = "read p; . <(cat); echo $p | " + cmd;

        // Construct the input: first line password, the rest
        // are env export statements
        let input = repo.password + "\n";
        for (const envKey of Object.keys(env)) {
            const envVal = util.quote(env[envKey]);
            input += `export ${envKey}=${envVal}\n`;
        }

        sshOptions.input = input;
    }
    else {
        sshOptions.input = repo.password;
    }

    return ssh(cmd, host, sshOptions);
}

export async function ssh(
    cmd: string, host: config.Host,
    options?: RunOptions): Promise<RunResult>
{
    if ( !options ) { options = {};  }

    let execCommand;
    if ( host.ssh === false ) {
        execCommand = cmd;
    }
    else {
        execCommand = typeof host.ssh === "string" ? host.ssh as string : "ssh -o 'BatchMode Yes'";
        cmd = util.quote("bash -c " + util.quote(cmd));
        execCommand += ` root@${host.name} ${cmd}`;
    }

    return command(execCommand, options);
}

export async function command(
    cmd: string,
    options?: RunOptions): Promise<RunResult>
{
    if ( !options ) { options = {};  }

    const process = ChildProcess.spawn("bash", ["-c", cmd]);

    // Feed input if given
    if ( options.input !== undefined ) {
        process.stdin.write(options.input);
        process.stdin.end();
    }

    // Output buffers
    let stdout: string = "";
    let stderr: string = "";

    // Capture stdout and stderr
    process.stdout.on("data", (data) => stdout += data);
    process.stderr.on("data", (data) => stderr += data);

    // Wait on process exit
    const exitCode = await processExit(process);

    const rv = {
        exitCode,
        stderr,
        stdout,
    };

    // We should be silent
    if ( options.silent ) { return rv; }

    // Handle exit code
    if ( exitCode !== 0 ) {
        logger.error(`Command exited with error (${exitCode})`);
    }

    if ( stdout ) { logger.info(stdout); }
    if ( stderr ) { logger.error(stderr); }

    return rv;
}
