import * as Ajv from "ajv";
import * as fs from "fs";
import * as yaml from "js-yaml";
import * as _ from "lodash";
import { promisify } from "util";
import * as logger from "./logger";
import * as restic from "./restic";
import * as util from "./util";
const readFile = promisify(fs.readFile);

const configMaxVersion = 2.0;

export interface ResticOptions {
    [key: string]: string | number | boolean;
}

export interface Env {
    [key: string]: string;
}

export type Repo = {

    name: string,
    env?: Env,
    url: string,
    "password-file"?: string,
    password?: string,
    restic?: ResticOptions,
};

export type Backup = {
    hostName: string,
    path: string,
    keep: BackupKeep,
    tags?: string | string[],
    repos: string[],
    restic?: ResticOptions,
    exclude?: string[],
};

export type Host = {
    name: string,
    backups: Backup[],
    env?: Env,
    pre?: string[],
    ssh?: string|boolean,
    arch?: string,
    restic?: ResticOptions,
    disabled?: boolean,
};

type BackupKeep = {
    last?: number,
    hourly?: number,
    daily?: number,
    weekly?: number,
    monthly?: number,
    yearly?: number,
    "within-duration"?: string,
};

type ConfigData = {
    version: string,
    repos: Repo[],
    hosts: Host[],
    "host-defaults"?: Host,
    "backup-defaults"?: Backup,
};

let configData: ConfigData;

export function repo(name: string): Repo {
    const repo = _.find(configData.repos, (repo) => repo.name === name );
    if ( !repo ) {
        throw new Error(`Unknown repo '${name}'\n`);
    }
    return repo;
}

export function repos(): Repo[] {
    return configData.repos || [];
}

export function host(name: string): Host {
    const host = _.find(configData.hosts, (host) => host.name === name );
    if ( !host ) {
        throw new Error(`Unknown host '${name}'\n`);
    }
    return host;
}

export function hosts(): Host[] {
    return configData.hosts || [];
}

export function backups(): Backup[] {
    return _.flatMap(configData.hosts, (host) => host.backups );
}

export function version(): string {
    return configData.version;
}

export async function middleware(argv: any): Promise<void> {
    if ( argv._ && argv._[0] === "repo" ) {
        logger.silent();
    }
    else if ( argv._ && argv._.length > 1 ) {
        throw new Error("Too many arguments\n");
    }

    const bufferLogs = !process.stdout.isTTY && !argv["sync-output"];

    if ( !logger.bufferIsEnabled() ) {
        logger.buffer(bufferLogs);
    }

    const pkg = require("../../package.json");
    logger.info(`Schedic: version ${pkg.version}`);

    await load(process.env.SCHEDIC_CONFIG || argv.config);

    logger.info(`Config: version ${configData.version}`);

    verify();
    await extend();
    applyArgs(argv);

    if ( backups().length === 0 ) {
        throw new Error("Config: no matching backups found\n");
    }
}

async function load(filename: string): Promise<void> {
    logger.info(`Config: loading '${filename}'`);
    configData = yaml.safeLoad(await readFile(filename));
}

async function extend(): Promise<void> {
    const hostDefaults = configData["host-defaults"];
    const backupDefaults = configData["backup-defaults"];

    for ( const host of configData.hosts ) {
        if ( host.env ) {
            host.env = await readEnvFileKeys(host.env);
        }

        for ( const key of Object.keys(hostDefaults) ) {
            const val = hostDefaults[key];

            if ( key === "restic" ) {
                host[key] = restic.mergeOptions(val, host[key]);
            }
            else if ( key === "env" ) {
                host[key] = await readEnvFileKeys(mergeEnv(val, host[key]));
            }
            else if ( ! (key in host) ) {
                host[key] = val;
            }

            for ( const backup of host.backups ) {
                backup.hostName = host.name;
                for ( const key of Object.keys(backupDefaults) ) {
                    const val = backupDefaults[key];

                    if ( key === "restic" ) {
                        backup[key] = restic.mergeOptions(val, backup[key]);
                    }
                    else if ( ! (key in backup) ) {
                        backup[key] = val;
                    }
                }
            }
        }
    }

    for ( const repo of configData.repos ) {
        if ( !repo.password ) {
            const password = await readFile(repo["password-file"]);
            repo.password = password.toString().replace(/\r?\n?$/, "");
        }
        if ( repo.env ) {
            repo.env = await readEnvFileKeys(repo.env);
        }
    }
}

function verify() {
    logger.info("Config: validating configuration");

    // Check supported version
    if ( parseFloat(configData.version) > configMaxVersion ) {
        throw new Error(`Config: version ${configData.version} not supported\n`);
    }

    // Do JSON Schema validation
    const ajv = new Ajv({ allErrors : true });
    const schema = require("../schema-config.json");
    const valid = ajv.validate(schema, configData);

    if ( !valid ) {
        logger.error("Config: schema validation failed");

        for (const error of ajv.errors ) {
            const add = JSON.stringify(error.params);
            logger.error("Config: " + error.dataPath + ": " + error.message + " | " + add);
        }

        throw new Error("Exiting on schema validation failure!\n");
    }

    // Let's do some semantic checks and collect all errors here
    const errors = [];
    const knownRepos = hash(repos(), "name");

    // Check uniqueness of host backup paths and if
    // backup.repos exists
    for ( const host of hosts() ) {
        const pathSeen = {};
        for ( const backup of host.backups ) {
            if ( pathSeen[backup.path] ) {
                errors.push(`multiple backup path '${backup.path}' for host '${host.name}'`);
            }
            pathSeen[backup.path] = true;

            if ( backup.repos ) {
                for ( const repoName of backup.repos ) {
                    if ( !knownRepos[repoName] ) {
                        errors.push(`unknown repo '${repoName}' for host '${host.name}'`);
                    }
                }
            }
        }
    }

    // Check repos in "backup-defaults"
    if ( configData["backup-defaults"] && configData["backup-defaults"].repos ) {
        for ( const repoName of configData["backup-defaults"].repos ) {
            if ( !knownRepos[repoName] ) {
                errors.push(`unknown repo '${repoName}' in "backup-defaults"'`);
            }
        }
    }

    // We require a repo password
    for ( const repo of repos() ) {
        if ( !repo.password && !repo["password-file"] ) {
            errors.push(`repo '${repo.name}' requires either "password" or "password-file" property`);
        }
    }

    // Print errors and exit if we had any
    if ( errors.length ) {
        for ( const error of errors ) {
            logger.error("Config: " + error);
        }
        throw new Error("Exiting on configuration errors!\n");
    }
}

function applyArgs(argv) {
    // Remove unwanted hosts
    const hosts = argv.host ? hash(argv.host) : null;

    configData.hosts = _.filter(configData.hosts, (host) => {
        if ( host.disabled ) { return false; }
        if ( hosts ) { return hosts[host.name]; }
        return true;
    });

    // Remove unwanted backup paths
    if ( argv.path ) {
        const paths = hash(argv.path);

        _.forEach(configData.hosts, (host) => {
            host.backups = _.filter(host.backups, (backup) => {
                return paths[backup.path];
            });
        });
    }

    // Remove unwanted repos
    if ( argv.repo ) {
        const repos = hash(argv.repo);

        configData.repos = _.filter(configData.repos, (repo) => {
            return repos[repo.name];
        });

        _.forEach(configData.hosts, (host) => {
            _.forEach(host.backups, (backup) => {
                backup.repos = _.filter(backup.repos, (repo) => {
                    return repos[repo];
                });
            });
        });

    }
}

function hash(array: any[], prop?: string) {
    const hash = {};

    _.map(array, (el) => {
        if ( prop ) {
            hash[el[prop]] = true;
        }
        else {
            hash[el] = true;
        }
    });

    return hash;
}

export function mergeEnv(base: Env, add: Env): Env {
    return util.mergeObjects(base as util.AnyObject, add as util.AnyObject);
}

async function readEnvFileKeys(env: Env): Promise<Env> {
    const fileReadEnv: Env = {};

    for ( const envKey of Object.keys(env) ) {
        if ( envKey.match(/^<</ ) ) {
            fileReadEnv[envKey.replace(/^<</, "")] =
                (await readFile(env[envKey])).toString().replace(/\r?\n?$/, "");
        }
        else {
            fileReadEnv[envKey] = env[envKey];
        }
    }

    return fileReadEnv;
}
