import chalk from "chalk";

let logBuffer: string[] = [];
let bufferEnabled: boolean = false;
let silenceEnabled: boolean = false;

process.on("exit", () => {
    if ( !silenceEnabled && bufferEnabled && logBuffer.length ) {
        console.log("> Log messages <");
        console.log("----------------");
        logBuffer.forEach((line) => {
            console.log(line);
        });
    }
});

export function silent(flag?: boolean) {
    if ( flag === undefined ) { flag = true; }
    silenceEnabled = flag;
}

export function print(line) {
    if ( silenceEnabled && bufferEnabled ) {
        log(line);
    }
    else if ( !silenceEnabled ) {
        console.log(line);
    }
}

export function info(msg: string): void {
    const prefix = chalk.grey(timestamp()) + chalk.green(" |INFO| ");
    logWithPrefix(prefix, msg);
}

export function error(msg: string): void {
    const prefix = chalk.grey(timestamp()) + chalk.red(" |ERR|  ");
    logWithPrefix(prefix, msg, chalk.red);
}

export function buffer(enabled?: boolean): void {
    if ( enabled === undefined ) { enabled = true; }
    bufferEnabled = enabled;
}

export function bufferIsEnabled(): boolean {
    return bufferEnabled;
}

export function clearBuffer(): void {
    logBuffer = [];
}

export function getBuffer(): string[] {
    return logBuffer;
}

function timestamp(): string {
    const time = new Date();
    return ("0" + time.getDate()).slice(-2) + "." +
           ("0" + (time.getMonth() + 1)).slice(-2) + "." +
           time.getFullYear() + " " +
           ("0" + time.getHours()).slice(-2) + ":" +
           ("0" + time.getMinutes()).slice(-2) + ":" +
           ("0" + time.getSeconds()).slice(-2);
}

function ident(x: any): any { return x; }

function logWithPrefix(prefix: string, msg: string, color = ident) {
    msg.split("\n").forEach( (line) => {
        log(prefix + color(line));
    });
}

function log(line) {
    if ( bufferEnabled ) {
        logBuffer.push(line);
    }
    else if ( !silenceEnabled ) {
        console.log(line);
    }
}
