import * as backup from "./backup";
import * as forget from "./forget";

module.exports = {
    command : "run",
    describe : "combined execution of 'backup' and 'forget'",
    handler,
};

export async function handler(argv) {
    await backup.handler(argv);
    await forget.handler(argv);
}
