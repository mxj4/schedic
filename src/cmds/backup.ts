import chalk from "chalk";
import * as _ from "lodash";
import * as config from "../lib/config";
import formatDuration from "../lib/format-duration";
import * as logger from "../lib/logger";
import printReport from "../lib/print-report";
import * as restic from "../lib/restic";
import * as run from "../lib/run";

module.exports = {
    command : "backup",
    describe : "perform backup of all or selected items",
    handler,
};

// CLI Handler
export async function handler(argv) {
    let hasErrors: number = 0;
    const report: string[][] = [
        [ "Host", "Type", "Repo", "Details", "ID", "Size", "Duration", "Result"],
    ];

    for ( const host of config.hosts() ) {
        hasErrors += await executePreCommands(host, argv, report);

        for ( const backup of host.backups ) {
            for ( const repoName of backup.repos ) {
                const exitCode = await performBackup(backup, config.repo(repoName), argv, report);
                hasErrors += exitCode;
            }
        }
    }

    printReport(report, "Backup summary", 0, {
        3: { width: 25, truncate: 25 },
        6: { alignment: "center" },
    });

    if ( hasErrors ) {
        throw new Error("Got errors during backup execution\n");
    }
}

// Perform pre commands of host
async function executePreCommands(host: config.Host, argv, report: string[][]): Promise<number> {
    if ( !host.pre ) { return 0; }

    const dry = argv["dry-run"] ? chalk.red(" (dry run)") : "";

    logger.info(`Backup${dry}: Executing pre-backup commands on host ${host.name}`);

    let hasErrors = 0;

    for ( const pre of host.pre ) {
        logger.info(`Backup${dry}: - ${pre}`);

        if ( dry ) {
            report.push([host.name, "Command", "", pre, "", "", "-", "Dry run"]);
            break;
        }

        const startTime = (new Date()).getTime();
        const rv = await run.ssh(pre, host);
        const stopTime = (new Date()).getTime();
        hasErrors += rv.exitCode;
        report.push([
            host.name,
            "Command",
            "",
            pre,
            "",
            "",
            formatDuration(stopTime - startTime),
            rv.exitCode ? "Error" : "Ok",
        ]);
    }

    return hasErrors;
}

// Perform one backup via ssh on configured host
async function performBackup(backup: config.Backup, repo: config.Repo, argv, report: string[][]): Promise<number> {
    const dry = argv["dry-run"] ? chalk.red(" (dry run)") : "";
    const host = config.host(backup.hostName);
    const cmd = restic.backupCommand(backup, repo);

    logger.info(`Backup${dry}: ${host.name}:${backup.path} to repo '${repo.name}'`);
    logger.info(`Backup${dry}: - ${cmd}`);

    if ( dry ) {
        report.push([host.name, "Backup", repo.name, backup.path, "-", "-", "-", "Dry run"]);
        return 0;
    }

    const startTime = (new Date()).getTime();
    const rv = await run.restic(cmd, host, repo);
    const stopTime = (new Date()).getTime();
    const error = rv.exitCode;

    const idMatch = rv.stdout.match(/snapshot\s+([a-f0-9]+)\s+saved/);
    const id = idMatch ? idMatch[1] : "";

    const sizeMatch = rv.stdout.match(/processed \d+ files, (.+?) in/);
    const size = sizeMatch ? sizeMatch[1] : "";

    report.push([
        host.name,
        "Backup: ",
        repo.name,
        backup.path,
        id,
        size,
        formatDuration(stopTime - startTime),
        error ? "Error" : "Ok",
    ]);

    return error;
}
