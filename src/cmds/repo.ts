import * as config from "../lib/config";

module.exports = {
    command : "repo",
    describe : "print shell code to set your env to a specific restic repo",
    handler,
};

export function handler(argv: any) {
    let repo: config.Repo;
    if ( argv._ && argv._.length === 2 ) {
        repo = config.repo(argv._[1]);
    }
    else if ( argv._ && argv._.length > 2 ) {
        throw new Error("Too many arguments\n");
    }
    else if ( !(config.repos().length === 1) ) {
        throw new Error("Missing repo argument\n");
    }
    else {
        repo = config.repos()[0];
    }

    let cmd = repo["password-file"] ?
        `export RESTIC_PASSWORD_FILE='${repo["password-file"]}'; ` :
        `export RESTIC_PASSWORD='${repo.password}'; `;

    cmd += `export RESTIC_REPOSITORY='${repo.url}'`;

    console.error(`Setting environment to repo '${repo.name}'`);
    console.log(cmd);
}
