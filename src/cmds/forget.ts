import chalk from "chalk";
import * as _ from "lodash";
import * as config from "../lib/config";
import formatDuration from "../lib/format-duration";
import * as logger from "../lib/logger";
import printReport from "../lib/print-report";
import * as restic from "../lib/restic";
import * as run from "../lib/run";

module.exports = {
    command : "forget",
    describe : "remove backups of all or selected items",
    handler,
};

// CLI Handler
export async function handler(argv) {
    let hasErrors: number = 0;

    const forgetReport: string[][] = [
        [ "Host", "Repo", "Path", "Kept", "Removed", "Result"],
    ];

    const reposSeen = {};
    let removed = 0;

    for ( const backup of config.backups() ) {
        for ( const repoName of backup.repos ) {
            const repo = config.repo(repoName);
            reposSeen[repoName] = { repo, hostName : backup.hostName };
            const rv = await performForget(backup, repo, argv, forgetReport);
            hasErrors += rv.error;
            removed += rv.removed;
        }
    }

    printReport(forgetReport, "Forget summary", 0, {
        2: { width: 25, truncate: 25 },
        5: { alignment: "center" },
    });

    if ( hasErrors ) {
        throw new Error("Got errors during forget execution\n");
    }

    if ( argv["skip-prune"] || (!argv["dry-run"] && removed === 0) ) {
        if ( !argv["skip-prune"] ) {
            logger.info("Nothing removed, skipping prune");
        }
        return 0;
    }

    const pruneReport: string[][] = [
        [ "Repo", "Duration", "Result" ],
    ];

    for ( const repoName of Object.keys(reposSeen) ) {
        const repoSeen = reposSeen[repoName];
        const host = config.host(repoSeen.hostName);
        const exitCode = await performPrune(repoSeen.repo, host, argv, pruneReport);
        hasErrors += exitCode;
    }

    printReport(pruneReport, "Prune summary", null, {
        2: { alignment: "center" },
    });

    if ( hasErrors ) {
        throw new Error("Got errors during prune execution\n");
    }
}
// Execute forget command via ssh on configured host
async function performForget(backup: config.Backup, repo: config.Repo, argv, report: string[][]): Promise<any> {
    const dry = argv["dry-run"] ? chalk.red(" (dry run)") : "";
    const host = config.host(backup.hostName);
    const cmd = restic.forgetCommand(backup, repo);

    logger.info(`Forget${dry}: ${host.name}:${backup.path} to repo '${repo.name}'`);
    logger.info(`Forget${dry}: - ${cmd}`);

    if ( !dry ) {
        const rv = await run.restic(cmd, host, repo);
        const keep = rv.stdout.match(/keep\s+(\d+)\s+snapshots/);
        const remove = rv.stdout.match(/remove\s+(\d+)\s+snapshots/);
        const error = rv.exitCode;

        report.push([
            host.name,
            repo.name,
            backup.path,
            (error ? "-" : keep ? keep[1] : "0"),
            (error ? "-" : remove ? remove[1] : "0"),
            error ? "Error" : "Ok",
        ]);

        const removed = remove ? parseInt(remove[1], 10) : 0;

        return { error, removed };
    }
    else {
        report.push([
            host.name,
            repo.name,
            backup.path,
            "-",
            "-",
            "Dry run",
        ]);

        return { error: 0, removed: 0 };
    }
}

// Execute forget command via ssh on configured host
async function performPrune(repo: config.Repo, host: config.Host, argv, report: string[][]): Promise<number> {
    const dry = argv["dry-run"] ? chalk.red(" (dry run)") : "";
    const cmd = restic.pruneCommand(repo);

    logger.info(`Prune${dry}: repo '${repo.name}' via host ${host.name}`);
    logger.info(`Prune${dry}: - ${cmd}`);

    if ( !dry ) {
        const startTime = (new Date()).getTime();
        const rv = await run.restic(cmd, host, repo);
        const stopTime = (new Date()).getTime();
        report.push([
            repo.name,
            formatDuration(stopTime - startTime),
            rv.exitCode ? "Error" : "Ok",
        ]);
        return rv.exitCode;
    }
    else {
        report.push([
            repo.name,
            "-",
            "Dry run",
        ]);
        return 0;
    }
}
